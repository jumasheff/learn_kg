# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-07-12 18:51
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('courses', '0004_auto_20160710_2103'),
    ]

    operations = [
        migrations.AddField(
            model_name='exercise',
            name='duration',
            field=models.PositiveSmallIntegerField(blank=True, default=0, verbose_name='Exercise duration in minutes'),
        ),
    ]
