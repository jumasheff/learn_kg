from django.apps import AppConfig


class   CoursesConfig(AppConfig):
    name = 'learnkg.courses'
    verbose_name = "Courses"
