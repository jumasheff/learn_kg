# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.conf.urls import url

from . import views

urlpatterns = [
    # URL pattern for the UserListView
    url(
        regex=r'^$',
        view=views.TracksListView.as_view(),
        name='tracks_list'
    ),
    url(
        regex=r'^track/(?P<track_id>\d+)$',
        view=views.UnitsListView.as_view(),
        name='units_list'
    ),
    url(
        regex=r'^unit/(?P<unit_id>\d+)$',
        view=views.LessonsListView.as_view(),
        name='lessons_list'
    ),
    url(
        regex=r'^lesson/(?P<lesson_id>\d+)$',
        view=views.ExerciseListView.as_view(),
        name='exercises_list'
    ),
    url(
        regex=r'^exercise/(?P<pk>\d+)/$',
        view=views.ExerciseTemplateView.as_view(),
        name='exercise'
    ),
    url(
        regex=r'^create_submission$',
        view=views.CreateSubmissionView.as_view(),
        name='create_submission'
    ),
]
